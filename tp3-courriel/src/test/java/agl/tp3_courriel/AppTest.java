package agl.tp3_courriel;


import org.junit.Test;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.runners.Parameterized;

import agl.tp3_courriel.Courrier;
import agl.tp3_courriel.EnvoiInvalide;

public class AppTest 
{
	private Courrier c;

	@BeforeEach
	public void init() {
		c = new Courrier();
	}
    
	/*
	
	//Test par ValueSource
	@ParameterizedTest
	@ValueSource(strings = {"adresse","adresse@mail","@mail.dm"}) // adresses incorrectes
	void testAdresses(String adresse) {
	    c.setAdresse(adresse);
        EnvoiInvalide erreur = assertThrows(EnvoiInvalide.class, () -> {c.envoyer();});
        assertTrue(erreur.getMessage().equals("Adresse du destinataire incorrecte"));
	}
	*/
	
	//Test par MethodSource
    @Parameterized.Parameters
    public static Stream<Arguments> adressesIncorrectes() {
        return Stream.of(
            Arguments.of("adresse", "Pas d'arobase"),
            Arguments.of("adresse@mail.", "Pas de nom de domaine"),
            Arguments.of("@mail.dm", "Rien avant l'arobase")
        );                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
    }
	
    @ParameterizedTest(name = "adresse testee : {0}, probleme renvoyé: {1}")
    @MethodSource("adressesIncorrectes")
    public void testAdresses(String adress, String problem) {
        c.setAdresse(adress);
        
        EnvoiInvalide erreur = assertThrows(EnvoiInvalide.class, () -> {c.envoyer();});
        assertTrue(erreur.getMessage().equals("Adresse du destinataire incorrecte"));
    }
	
    
    
    
    
    
    
    
    
    
    
    
    //envoyer sans adresse
    @Test
    public void sansAdresse()
    {
        EnvoiInvalide erreur = assertThrows(EnvoiInvalide.class, () -> {c.envoyer();});
        assertTrue(erreur.getMessage().equals("Pas de destinataire"));
    }
    //Test adresse incorrectes
    
    //Sans arobase
    @Test
    public void adresseIncorrecteCas1()
    {
        c.setAdresse("adresse");
        
        EnvoiInvalide erreur = assertThrows(EnvoiInvalide.class, () -> {c.envoyer();});
        assertTrue(erreur.getMessage().equals("Adresse du destinataire incorrecte"));
    }
    
    //sans nom de domaine
    @Test
    public void adresseIncorrecteCas2()
    {
        c.setAdresse("adresse@mail.");
        
        EnvoiInvalide erreur = assertThrows(EnvoiInvalide.class, () -> {c.envoyer();});
        assertTrue(erreur.getMessage().equals("Adresse du destinataire incorrecte"));
    }
    
    
    //rien avant l'arobase
    @Test
    public void adresseIncorrecteCas3()
    {
        c.setAdresse("@mail.dm");
        
        EnvoiInvalide erreur = assertThrows(EnvoiInvalide.class, () -> {c.envoyer();});
        assertTrue(erreur.getMessage().equals("Adresse du destinataire incorrecte"));
    }
    
    //Test mail sans titre
    @Test
    public void envoiSansTitre()
    {
        c.setAdresse("adresse@mail.dm");
        
        EnvoiInvalide erreur = assertThrows(EnvoiInvalide.class, () -> {c.envoyer();});
        assertTrue(erreur.getMessage().equals("Pas de titre"));
    }
    
    //Test absence de piece jointe
    @Test
    public void envoiSansFichierJointCas1()
    {
        c.setAdresse("adresse@mail.dm");
        c.setTitre("titre mail");
        c.setCorps("fichier joint");
        
        EnvoiInvalide erreur = assertThrows(EnvoiInvalide.class, () -> {c.envoyer();});
        assertTrue(erreur.getMessage().equals("Pas de piece jointe"));
    }
    
    @Test
    public void envoiSansFichierJointCas2()
    {
        c.setAdresse("adresse@mail.dm");
        c.setTitre("titre mail");
        c.setCorps("piece jointe");
        
        EnvoiInvalide erreur = assertThrows(EnvoiInvalide.class, () -> {c.envoyer();});
        assertTrue(erreur.getMessage().equals("Pas de piece jointe"));
    }
    
    @Test
    public void envoiSansFichierJointCas3()
    {
        c.setAdresse("adresse@mail.dm");
        c.setTitre("titre mail");
        c.setCorps("mail avec PJ");
        
        EnvoiInvalide erreur = assertThrows(EnvoiInvalide.class, () -> {c.envoyer();});
        assertTrue(erreur.getMessage().equals("Pas de piece jointe"));
    }
    
    //Adresse fonctionelle
    @Test
    public void envoiAdresseCorrecte() {
        c.setAdresse("adresse@eee.domaine");
        c.setTitre("titre generique");
        c.setCorps("mail avec PJ");
        c.addJoint("image_test.png");
        
        try {
        	c.envoyer();
        } catch (Exception e) {
        	fail(e);
        }
    }
}
