package couvertureMutationTP4;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;

import couvertureMutationTP4.SUT;
import couvertureMutationTP4.TableauPleinException;
import couvertureMutationTP4.TableauVideException;

class TestSUT {

	//Survie de mutant sur la ligne 36 dans SUT
	//On va corriger cela en modifiant le code et en ajoutant un test pour l'égalité afin d'avoir une couverture de 100%
	//et pour supprimer le mutant survivant
	@Test
	void testAjoutDe5éléments() throws TableauPleinException {
		SUT sut=new SUT(5);
		sut.ajout(1);
		sut.ajout(2);
		sut.ajout(3);
		sut.ajout(4);
		sut.ajout(5);
		int[] tab=sut.values();
		int[] expected = {1,2,3,4,5};
		assertArrayEquals(expected,tab);
		assertEquals(5,sut.values().length);
	}

	@Test 
	void testExceptionQuandAjoutDansTableauPlein() throws TableauPleinException {
		SUT sut=new SUT(3);
		sut.ajout(1);
		sut.ajout(2);
		sut.ajout(3);
		assertThrows(TableauPleinException.class, ()->{sut.ajout(4);});
	}
	
	@Test
	void testMinTableauRempliTrié() throws TableauPleinException, TableauVideException {
		SUT sut=new SUT(3);
		sut.ajout(1);
		sut.ajout(2);
		sut.ajout(3);
		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertFalse(ArrayUtils.contains(sut.values(), 1));
		assertEquals(2,sut.values().length);
	}

	@Test
	void testMinTableauUnElement() throws TableauPleinException, TableauVideException {
		SUT sut=new SUT(3);
		sut.ajout(1);
		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertFalse(ArrayUtils.contains(sut.values(), 1));
		assertEquals(0,sut.values().length);
	}
	
	@Test
	void testMinTableauEgal() throws TableauPleinException, TableauVideException {
		SUT sut=new SUT(3);
		sut.ajout(1);
		sut.ajout(3);
		sut.ajout(1);
		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertEquals(2,sut.values().length);
		int[] tab=sut.values(); //valeur dans le tableau execution
		int[] expected = {3,1}; //valeurs attendues dans le tableau
		assertArrayEquals(expected,tab);
	}
	
	@Test
	void testMinTableauRempliNonTrié() throws TableauPleinException, TableauVideException {
		SUT sut=new SUT(3);
		sut.ajout(5);
		sut.ajout(1);
		sut.ajout(2);
		assertEquals(1,sut.retourneEtSupprimePremiereOccurenceMin());
		assertFalse(ArrayUtils.contains(sut.values(), 1));
		assertEquals(2,sut.values().length);
	}
	
	@Test
	void testMinTableauVide() {
		SUT sut=new SUT(3);
		assertThrows(TableauVideException.class, ()->{sut.retourneEtSupprimePremiereOccurenceMin();});
	}
	
	
}
