package agl.tp3_Poste;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import agl.tp3_Poste.ColisExpress;
import agl.tp3_Poste.ColisExpressInvalide;
import agl.tp3_Poste.Recommandation;

public class TestColisExpress {
    private static float tolerancePrix=0.001f;
    
    //On teste l'affranchissement
    @Test
    public void testAffranchissement() {
        try {
            ColisExpress colisExpress1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 25, 0.02f, Recommandation.deux, "train electrique", 200, true);
	    assertEquals(colisExpress1.tarifAffranchissement(), 33.0f, tolerancePrix);
        } catch(Exception e) {
            assertTrue(false);
        }
    }

    //On teste si la valeur de la methode toString est correcte
    @Test 
    public void testToString() {
        try {
            ColisExpress colisExpress1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 25, 0.02f, Recommandation.deux, "train electrique", 200, true);
            assertTrue(colisExpress1.toString().equals("Colis express 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0/25.0/0"));
        } catch(Exception e) {
            assertTrue(false);
        }
    }
    
    //on teste si la date d'envoi est incoherent par rapport à la date locale
    @Test
    public void date() {
    	try {
			ColisExpress colisExpress1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 25, 0.02f, Recommandation.deux, "train electrique", 200, true);
			assertEquals(LocalDate.now(),colisExpress1.getDateEnvoi());
    	} catch (ColisExpressInvalide e) {
			e.printStackTrace();
			assertTrue(false);
		}
    }
    
    //On teste le tarif de remboursement
    @Test
    public void remboursement() {
        try {
            ColisExpress colisExpress1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 25, 0.02f, Recommandation.deux, "train electrique", 200, true);
	    assertEquals(colisExpress1.tarifRemboursement(), 100.0f, tolerancePrix);
        } catch(Exception e) {
            assertTrue(false);
        }
    }
    
    //On test si le poids dépasse 30 kg
    @Test
    public void poindInvalide() {
        try {
            ColisExpress colisExpress1 = new ColisExpress("Le pere Noel", "famille Kaya, igloo 10, terres ouest", "7877", 31, 0.02f, Recommandation.deux, "train electrique", 200, true);
            assertTrue(false);
            colisExpress1.affiche();
        } catch(Exception e) {
            assertTrue(e.getMessage().equals("poids incohérent, votre colis ne pourra pas être acheminé."));
        }
    }
    
}
