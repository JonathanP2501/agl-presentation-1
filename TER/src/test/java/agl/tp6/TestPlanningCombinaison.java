package agl.tp6;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class TestPlanningCombinaison {

	public PlanningCombinaison pc;
	
	@BeforeEach
	public void init() {
		pc = new PlanningCombinaison();
	}
	
	@Test
	void testPCsetProf() {
		String prof = "Michel Dupont";
		pc.setProf(prof);
		assertEquals(prof, pc.getProf());
	}
	
	@Test
	void testPCsetGroupe() {
		Groupe g = new Groupe("groupe");
		pc.setGroupe(g);
		assertEquals(g, pc.getGroupe());
	}
	
	@Test
	void testPCsetDate() {
		LocalDateTime t = LocalDateTime.now();
		pc.setDate(t);
		assertEquals(t, pc.getDate());
	}
	
	@Test
	void testPCtoString() {
		Sujet s = new Sujet("sujet");
		Groupe g = new Groupe("groupe");
		g.setAffectation(s);
		LocalDateTime t = LocalDateTime.now();
		String p = "Michel Dupont";
		pc.setProf(p);
		pc.setDate(t);
		pc.setGroupe(g);
		assertEquals(t.toString()+": "+p+" encadre "+g.getNom()+" (sujet: "+g.getAffectation().getTitre()+" )", pc.toString());
	}
}
