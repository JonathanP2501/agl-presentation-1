package agl.tp6;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import agl.tp6.exception.AffectationVoeuImpossibleException;
import agl.tp6.exception.AffectationImpossibleException;
import agl.tp6.exception.TailleInvalideException;
import agl.tp6.hongrois.Hongrois;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TestGestionTer {
	
	private Groupe g1;
	private Groupe g2;
	private Groupe g3;
	private Sujet s1;
	private Sujet s2;
	private Sujet s3;
	private String p1;
	private String p2;
	private String p3;
	
	@Mock
    Hongrois hongrois;
	
	@InjectMocks
	GestionTER gestion = new GestionTER();
	
	@BeforeEach
	void setup() {
		g1 = new Groupe("groupe1");
		g2 = new Groupe("groupe2");
		g3 = new Groupe("groupe3");
		s1 = new Sujet("sujet1");
		s2 = new Sujet("sujet2");
		s3 = new Sujet("sujet3");
		p1 = "Michel Dupont";
		p2 = "Jacqueline Bonnaire";
		p3 = "Esteban Boanca";
		try {
			g1.addVoeu(1, s1);
			g1.addVoeu(3, s2);
			g2.addVoeu(4, s3);
			g3.addVoeu(1, s2);
			g3.addVoeu(5, s3);
		} catch (AffectationVoeuImpossibleException e) {
			e.printStackTrace();
		}
	}
	
	@Test
	void testGestionTERlancerAffectation() {
		gestion.addGroupe(g1);
		gestion.addGroupe(g2);
		gestion.addGroupe(g3);
		try {
			when(hongrois.affectation(1)).thenReturn(null);
		} catch (TailleInvalideException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		AffectationImpossibleException thrown = assertThrows(AffectationImpossibleException.class, () -> {gestion.lancerAffectation(1);});
        assertEquals("Resultat du hongrois incongru", thrown.getMessage());
	}
	
	@Test
	void testGestionTERsetSujets() {
		ArrayList<Sujet> l = new ArrayList<Sujet>();
		l.add(s1);
		l.add(s2);
		gestion.setSujets(l);
		assertEquals(l,gestion.getSujets());
	}
	
	@Test
	void testGestionTERaddSujets() {
		ArrayList<Sujet> l = new ArrayList<Sujet>();
		l.add(s1);
		l.add(s2);
		gestion.addSujet(s1);
		gestion.addSujet(s2);
		assertEquals(l,gestion.getSujets());
	}
	
	@Test
	void testGestionTERsetProfs() {
		ArrayList<String> l = new ArrayList<String>();
		l.add(p1);
		l.add(p2);
		gestion.setProfs(l);
		assertEquals(l,gestion.getProfs());
	}
	
	@Test
	void testGestionTERgetGroupe() {
		Groupe g4 = gestion.getGroupe(15555);
		assertEquals("n'existe pas", g4.getNom());
	}
}
