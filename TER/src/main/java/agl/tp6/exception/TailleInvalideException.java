package agl.tp6.exception;

public class TailleInvalideException extends Exception {
    public TailleInvalideException(String string) {
        super(string);
    }
}
