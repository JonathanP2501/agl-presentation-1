package agl.tp6.exception;

/**
 * Classe d'exception CantAddVoeu
 * lancée par un ajout incorrect de voeu dans la classe groupe
 */
public class AffectationVoeuImpossibleException extends Exception {

	public AffectationVoeuImpossibleException(String string) {
		super(string);
	}

}
