package agl.tp6.exception;

/**
 * Classe d'exception CantAffect
 * lancée par une affectation ratée du hongrois
 */
public class AffectationImpossibleException extends Exception {
	public AffectationImpossibleException(String string) {
		super(string);
	}
}