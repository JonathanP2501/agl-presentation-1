package agl.tp6;
import com.github.javafaker.Faker;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Cette classe permet de générer un affichage html des affectations groupe/sujet contenu dans un objet GestionTER.
 * @version 1.0
 * @since 2021-12-04
 */

public class AffichageAffectation {
    /**
     * Permet de créer un fichier html contenant un tableau des affectations groupe/sujet.
     * @param ter objet GestionTER contenant les affectations groupe/sujet.
     * @param nomFichierHtml nom du fichier html qui sera écrit en sorti. Ce fichier html contiendra un tableau des
     * affectaions groupe/sujet.
     */
    public static void affectationsVersHtml(GestionTER ter, String nomFichierHtml) {
        try {
            File fichierAffichageHtml = new File(nomFichierHtml);
            FileWriter redacteurDeFichier = new FileWriter(nomFichierHtml);
            String htmlCode = "<style type=\"text/css\">\n" +
                    ".tftable {font-size:12px;color:#333333;width:100%;border-width: 1px;border-color: #729ea5;border-collapse: collapse;}\n" +
                    ".tftable th {font-size:12px;background-color:#acc8cc;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;text-align:left;}\n" +
                    ".tftable tr {background-color:#d4e3e5;}\n" +
                    ".tftable td {font-size:12px;border-width: 1px;padding: 8px;border-style: solid;border-color: #729ea5;}\n" +
                    ".tftable tr:hover {background-color:#ffffff;}\n" +
                    "</style>";
            htmlCode += "<table class=\"tftable\" border=\"1\">\n";

            String htmlCodeHeader = "<tr><th>Nom des groupes:</th>";
            String htmlCodeRow = "<tr><th>Nom du sujet affecté:</th>";
            for(Groupe groupe : ter.getGroupes()){
                htmlCodeHeader += "<th>" + groupe.getNom() + "</th>";
                if(groupe.getAffectation() != null) {
                    htmlCodeRow += "<td>" + groupe.getAffectation() + "</td>";
                } else {
                    htmlCodeRow += "<td></td>";
                }
            }
            htmlCodeHeader += "</tr>\n";
            htmlCodeRow += "</tr>\n";
            htmlCode += htmlCodeHeader + htmlCodeRow + "</table>";
            redacteurDeFichier.write(htmlCode);
            redacteurDeFichier.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Permet de générer un objet GestionTER aléatoirement. Cet objet lie chaque groupe à un sujet.
     * @param nombreDeGroupe nombre de groupe contenu dans gestionTER et généré aléatoirement.
     * @return GestionTER retourne un objet gestionTER dont chaque groupe est lié à un sujet.
     */
    public static GestionTER generationAleatoireGestionTer(int nombreDeGroupe){
        GestionTER gestionTER = new GestionTER();
        for(int i = 0; i < nombreDeGroupe; ++i) {
            Faker faker = new Faker();
            Groupe g = new Groupe(faker.cat().name());
            Sujet s = new Sujet(faker.book().title());
            gestionTER.addGroupe(g);
            gestionTER.addSujet(s);
            g.setAffectation(s);
        }
        return gestionTER;
    }
}

