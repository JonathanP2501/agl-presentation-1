package agl.tp6;

import java.time.LocalDateTime;

public class PlanningCombinaison {
	//attributs
	private String prof;
	private Groupe groupe;
	private LocalDateTime date;
	
	//constructeur
	public PlanningCombinaison() {};
	
	//getter/setter
	public String getProf() {
		return prof;
	}
	public void setProf(String prof) {
		this.prof = prof;
	}
	public Groupe getGroupe() {
		return groupe;
	}
	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}
	public LocalDateTime getDate() {
		return date;
	}
	public void setDate(LocalDateTime date) {
		this.date = date;
	}
	
	//méthodes
  /**
   * toString
   * @return String instance bien formée
   */
	public String toString() {
		return date.toString() + ": " + prof + " encadre " + groupe.getNom() + " (sujet: " + groupe.getAffectation().getTitre() + " )";
	}
}